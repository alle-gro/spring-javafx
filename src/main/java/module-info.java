open module testfx.client {
    requires java.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires spring.boot.autoconfigure;
    requires spring.boot;
    requires spring.context;
    requires spring.beans;
    requires java.annotation;
    requires spring.core;
    requires java.persistence;
    requires spring.data.commons;

    exports app;
}

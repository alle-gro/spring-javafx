package app;

import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

public abstract class AbstractJavaFxApplication  extends Application {
    protected ConfigurableApplicationContext context;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Spring context initialization in FX thread
        context = SpringApplication.run(getClass(), getParameters().getRaw().toArray(new String[0]));
        context.getAutowireCapableBeanFactory().autowireBean(this);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        context.close();
        System.out.println("AbstractJavaFxApplication.stop()");
    }


}

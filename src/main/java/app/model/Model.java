package app.model;

import org.springframework.stereotype.Component;

@Component
public class Model  {
    private String title = "Model";
    private String tab1Text = "Tab 1 pane";
    private String tab2Text = "Tab 2 pane";

    public Model(String title, String tab1Text, String tab2Text) {
        this.setTitle(title);
        this.setTab1Text(tab1Text);
        this.setTab2Text(tab2Text);
    }
    public Model() {
        this.setTitle(title);
        this.setTab1Text(tab1Text);
        this.setTab2Text(tab2Text);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getTab1Text() {
        return tab1Text;
    }

    public void setTab1Text(String tab1Text) {
        this.tab1Text = tab1Text;
    }

    public String getTab2Text() {
        return tab2Text;
    }

    public void setTab2Text(String tab2Text) {
        this.tab2Text = tab2Text;
    }
}

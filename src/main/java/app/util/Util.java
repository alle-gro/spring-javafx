package app.util;

import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.nio.charset.Charset;
import java.util.Random;

public class Util {

    public static String  getRandomString() {
        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);

        return byteArrayToHex(array);
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for(byte b: a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public static void setStageIcon(Stage stage, String title) {
        stage.getIcons().add(new Image("/images/mainwindow-icon.png"));
        stage.setTitle(title);

    }
}

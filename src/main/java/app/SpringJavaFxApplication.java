package app;

import app.controller.tabWidget1.TabPaneRootController1;
import app.controller.mainwindow.MainWindowController;
import app.controller.tabWidget3.TabPaneRootControl;
import app.model.Model;
import app.util.Util;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringJavaFxApplication extends AbstractJavaFxApplication {
private static final int WINDOW_SIZE_CORRECTION = 100;
    @Value("${window.title:Spring JavaFx application:}")
    private String windowTitle;

    @Autowired
    MainWindowController mainWindowController;

    @Override
    public void start(Stage primaryStage) throws Exception {
        super.start(primaryStage);
        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
        double width = visualBounds.getWidth() - WINDOW_SIZE_CORRECTION;
        double height = visualBounds.getHeight() - WINDOW_SIZE_CORRECTION;

        primaryStage.setScene(new Scene(mainWindowController.getRootNode(), width, height));
        primaryStage.centerOnScreen();
        mainWindowController.setStage(primaryStage);
        Util.setStageIcon(primaryStage, windowTitle);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }











    // -----------------------------------------------------------------------
    public void createTabPane1(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/tabWidget1/tabPaneRootView.fxml"));
        Model model = new Model("TabPane header", "TabPage 1", "TabPage 2");

        Parent root = loader.load();

        TabPaneRootController1 controller = loader.getController();
        controller.setModel(model);

        Scene scene = new Scene(root, 300, 275);
        stage.setTitle("TabPane Test  Application:");
        stage.setScene(scene);
        stage.show();
    }

    public void createTabPane2(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/mainwindow/mainWindowView.fxml"));
        Model model = new Model("TabPane header", "TabPage 1", "TabPage 2");

        //loader.setControllerFactory((type -> new MainWindowController(model)));

        Parent root = loader.load();

        Scene scene = new Scene(root, 300, 275);
        stage.setTitle("TabPane Test  Application:");
        stage.setScene(scene);
        stage.show();
    }

    public void createTabPane3(Stage stage) throws Exception {
        Model model = new Model("TabPane header", "TabPage 1", "TabPage 2");

        VBox vbox = new VBox();
        TabPaneRootControl tabPane = new TabPaneRootControl(model);
        Label label = new Label(model.getTitle());

        vbox.getChildren().add(label);
        vbox.getChildren().add(tabPane);
        Scene scene = new Scene(vbox, 300, 275);

        stage.setTitle("TabPane Test  Application:");
        stage.setScene(scene);
        stage.show();
    }
}

package app.controller.about;

import app.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;

@Component
public class AboutController {
    @Autowired
    private ConfigurableApplicationContext context;
    private Stage stage;

    @FXML
    Button buttonOk;

    @PostConstruct
    public void init()  throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/about/aboutView.fxml"));
        loader.setControllerFactory(context::getBean);
        Parent rootNode = loader.load();
        stage.setScene(new Scene(rootNode));
    }

    public AboutController() {
        stage = new Stage();
        stage.centerOnScreen();
        stage.initModality(Modality.APPLICATION_MODAL);
        Util.setStageIcon(stage, "About: ");
    }

    @FXML
    public void initialize() {
        System.out.println("! AboutController.initialize()");

        buttonOk.setOnAction(this::okButtonClick);
    }

    public void show() {
        stage.showAndWait();

        // process result of stage operation.
    }

    public void okButtonClick(ActionEvent actionEvent) {
        stage.close();
    }
}

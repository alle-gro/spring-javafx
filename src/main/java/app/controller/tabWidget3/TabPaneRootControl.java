package app.controller.tabWidget3;

import app.model.Model;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Class.*;

public class TabPaneRootControl extends TabPane {

    private final Model model;

    public TabPaneRootControl(Model model) {
       this.model = model;
       init();
       addTabs();
    }


    public TabPaneRootControl() {
        this.model = null;
    }

    private void init() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/view/tabWidget3/tabPaneRootView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    private void addTabs() {
        List<Tab> tabs  = new ArrayList(new ArrayList());

        getTabs().forEach( t-> {
            try {
                Class<?> c = forName(t.getClass().getName());
                Constructor<?> constructor = c.getConstructor(Model.class);
                Tab tab = (Tab)constructor.newInstance(model);
                tab.setId(t.getId());
                tab.setText(t.getText());
                tabs.add(tab);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        getTabs().clear();
        getTabs().addAll(tabs);
    }

    @FXML
    public void initialize() {
        System.out.println("!!! initialize");

        this.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
                    if ( "tabPage1".equals((newValue != null)? newValue.getId(): null)) {
                        System.out.println("- Tab 2 -");
                    } else if ("tabPage2".equals((newValue != null)? newValue.getId(): null)) {
                        System.out.println("- Tab 1 -");
                    }
                });
    }
}

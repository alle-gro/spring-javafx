package app.controller.tabWidget3;

import app.model.Model;
import app.util.Util;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;

import java.io.IOException;

public class TabPage2Control extends Tab {
    private  final Model model;

    @FXML
    private Label tab2_label;

    public TabPage2Control(Model model) {
        this.model = model;
        init();
    }

    public TabPage2Control() {
        this.model = null;
    }

    public Model getModel() {
        return model;
    }

    @FXML
    public void initialize() {
        tab2_label.setText(getModel().getTab2Text());
    }

    @FXML
    public void handleTab2Button() {
        System.out.println(Util.getRandomString());
        tab2_label.setText(getModel().getTab2Text() + " : " +  Util.getRandomString());
    }

    private void init() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/view/tabWidget3/tabPage2View.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
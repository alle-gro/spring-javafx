package app.controller.tabWidget3;

import app.model.Model;
import app.util.Util;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;

import java.io.IOException;

public class TabPage1Control extends Tab {
    private  final Model model;

    private StringProperty source;

    @FXML
    private Label tab1_label;

    public TabPage1Control(Model model) {
        this.model = model;
        init();
    }

    public TabPage1Control() {
        this.model = null;
    }

    public Model getModel() {
        return model;
    }

    @FXML
    public void initialize() {
        tab1_label.setText(getModel().getTab1Text());
    }

    @FXML
    public void handleTab1Button() {
        System.out.println(Util.getRandomString());
        tab1_label.setText(getModel().getTab1Text() + " : " +  Util.getRandomString());
    }

    private void init() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/view/tabWidget3/tabPage1View.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
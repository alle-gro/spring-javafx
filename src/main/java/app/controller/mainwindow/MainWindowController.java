package app.controller.mainwindow;

import app.context.SpringContext;
import app.controller.childwindow.ChildWindowController;
import app.domain.Contact;
import app.model.Model;
import app.service.ContactService;
import app.util.Util;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class MainWindowController {
    private Parent rootNode;
    private Stage stage;

    @Autowired
    private Model model;

    @Autowired
    private ContactService contactService;

    @Autowired
    private ConfigurableApplicationContext context;

    @FXML private TabPane tabPane;
    @FXML private TableView<Contact> table;

    @FXML TextField txtName;
    @FXML TextField txtPhone;
    @FXML TextField txtEmail;
    @FXML Button updButton;
    @FXML Button addButton;
    @FXML Button deleteButton;

    private ObservableList<Contact> data;

    @FXML private Tab tabPage1;
    @FXML private Tab tabPage2;

    @FXML Label tab2_label;


    @FXML
    public void initialize() {
        System.out.println("! MainWindowController.initialize()");

        tab2_label.setText(model.getTab2Text());

        tabPane.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == tabPage2) {
                System.out.println("- Tab 2 -");
            } else if (newValue == tabPage1) {
                System.out.println("- Tab 1 -");
            }
        });


        populateTable();
        addTableListeners();

    }

    @FXML
    public void addContact(ActionEvent actionEvent) {
        if (!txtName.getText().isBlank()) {
            Contact contact = new Contact(txtName.getText(), txtPhone.getText(), txtEmail.getText());
            contactService.save(contact);
            data.add(contact);

            // clear fields
            txtName.setText("");
            txtPhone.setText("");
            txtEmail.setText("");
        }
    }

    @FXML
    public void deleteContact(ActionEvent actionEvent) {
        if (!(table.getSelectionModel() == null || table.getSelectionModel().getSelectedItem() == null)) {
            Contact contact  = table.getSelectionModel().getSelectedItem();

            contactService.delete(contact);
            data.remove(contact);
        }
    }

    @FXML
    public void updateContact(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String text = button.getText();
        if (!(table.getSelectionModel() == null || table.getSelectionModel().getSelectedItem() == null)) {
            if ("Update".equalsIgnoreCase(text)) {

                Contact contact = table.getSelectionModel().getSelectedItem();
                txtName.setText(contact.getName());
                txtPhone.setText(contact.getPhone());
                txtEmail.setText(contact.getEmail());
                addButton.setDisable(true);
                deleteButton.setDisable(true);
                button.setText("Save");
            }
            else {
                Contact contact = table.getSelectionModel().getSelectedItem();

                contact.setName(txtName.getText());
                contact.setPhone(txtPhone.getText());
                contact.setEmail(txtEmail.getText());

                int itemIndex = data.indexOf(contact);
                if (itemIndex != -1) {
                    data.set(itemIndex, contact);
                }

                contactService.save(contact);

                txtName.setText("");
                txtPhone.setText("");
                txtEmail.setText("");
                addButton.setDisable(false);
                deleteButton.setDisable(false);
                button.setText("Update");
            }
        }
    }

    @FXML
    public void handleTab2Button(ActionEvent actionEvent) {
        System.out.println(Util.getRandomString());
        tab2_label.setText(model.getTab2Text() + " : " +  Util.getRandomString());
    }

    @PostConstruct
    public void init() throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/mainwindow/mainWindowView.fxml"));
        loader.setControllerFactory(context::getBean);
        rootNode = loader.load();
    }

    public Parent getRootNode() {
        return rootNode;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private void populateTable() {
        List<Contact> contacts = contactService.findAll();
        data = FXCollections.observableArrayList(contacts);

        // Add table columns
        TableColumn<Contact, String> idColumn = new TableColumn<>("ID");
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<Contact, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Contact, String> phoneColumn = new TableColumn<>("Phone");
        phoneColumn.setCellValueFactory(new PropertyValueFactory<>("phone"));

        TableColumn<Contact, String> emailColumn = new TableColumn<>("E-mail");
        emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));

        table.getColumns().setAll(idColumn, nameColumn, phoneColumn, emailColumn);

        // Populate table by data
        table.setItems(data);
    }

    private void addTableListeners() {
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                if ("Save".equals(updButton.getText())) {
                    Contact contact = table.getSelectionModel().getSelectedItem();
                    txtName.setText(contact.getName());
                    txtPhone.setText(contact.getPhone());
                    txtEmail.setText(contact.getEmail());
                }
            }
        });
    }

}

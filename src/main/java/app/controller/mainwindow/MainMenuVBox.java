package app.controller.mainwindow;

import app.context.SpringContext;
import app.controller.about.AboutController;
import app.controller.childwindow.ChildWindowController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.InputEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;


import java.io.IOException;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;


public class MainMenuVBox extends VBox {
    @FXML
    private MenuBar menuBar;

    @Autowired
    private AboutController aboutController;

    private Parent rootNode;


    @Autowired
    private ConfigurableApplicationContext context;

    public MainMenuVBox() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(
                "/view/mainwindow/mainMenuView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void initialize(java.net.URL arg0, ResourceBundle arg1) {
        System.out.println("! MainMenuVBox.initialize()");

    }

    /**
     * Handle action related to input (in this case specifically only responds to
     * keyboard event CTRL-A).
     *
     * @param event Input event.
     */
    @FXML
    private void handleKeyInput(final InputEvent event)
    {
        System.out.println("!!!");
        if (event instanceof KeyEvent)
        {
            final KeyEvent keyEvent = (KeyEvent) event;
            if (keyEvent.isControlDown() && keyEvent.getCode() == KeyCode.A)
            {
                provideAboutFunctionality();
            }
        }
    }

    public void handleNewWindowAction(ActionEvent actionEvent) {
        MenuItem mItem = (MenuItem) actionEvent.getSource();
        String id = mItem.getId();
        if ("new".equalsIgnoreCase(id)) {
            ChildWindowController tabWindow = SpringContext.getBean(ChildWindowController.class);
            tabWindow.show();
        }
    }

    @FXML
    public void handleExitAction(ActionEvent actionEvent) {
        Platform.exit();
        System.exit(0);
    }

    @FXML
    public void handleAboutAction(ActionEvent actionEvent) {
        provideAboutFunctionality();
    }

    /**
     * Perform functionality associated with "About" menu selection or CTRL-A.
     */
    private void provideAboutFunctionality()
    {
        System.out.println("!!!");

        AboutController aboutDialog = SpringContext.getBean(AboutController.class);
        aboutDialog.show();
    }



}


package app.controller.tabWidget1;

import app.model.Model;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

public class TabPaneRootController1 {

    private Model model;

    @FXML
    private TabPane tabPane;

    @FXML
    private Label tabPaneLabel;

    // Inject tab content
    @FXML private Tab tabPage1;
    @FXML private Tab tabPage2;

    // Inject tab controllers
    @FXML private TabPage1Controller tab_page_1Controller;
    @FXML private TabPage2Controller tab_page_2Controller;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
        tab_page_1Controller.setModel(model);
        tab_page_2Controller.setModel(model);

        tabPaneLabel.setText(model.getTitle());
    }

    @FXML
    public void initialize() {
        System.out.println("!!! initialize");

        tabPane.getSelectionModel().selectedItemProperty().addListener(
                (ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) -> {
            if (newValue == tabPage2) {
                System.out.println("- Tab 2 -");
            } else if (newValue == tabPage1) {
                System.out.println("- Tab 1 -");
            }
        });
    }
}

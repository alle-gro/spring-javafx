package app.controller.tabWidget1;

import app.model.Model;
import app.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class TabPage1Controller {
    private  Model model;

    @FXML
    private Label tab1_label;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
        tab1_label.setText(getModel().getTab1Text());
    }

    @FXML
    public void initialize() {

    }

    public void handleTab1Button() {
        System.out.println(Util.getRandomString());
        tab1_label.setText(getModel().getTab1Text() + " : " +  Util.getRandomString());
    }

}
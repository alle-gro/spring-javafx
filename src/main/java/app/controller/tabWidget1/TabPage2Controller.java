package app.controller.tabWidget1;

import app.model.Model;
import app.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class TabPage2Controller {
    private Model model;

    @FXML
    private Label tab2_label;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
        tab2_label.setText(getModel().getTab2Text());
    }

    @FXML
    public void initialize() {

    }

    public void handleTab2Button() {
        tab2_label.setText(getModel().getTab2Text() + " : " +  Util.getRandomString());
    }
}
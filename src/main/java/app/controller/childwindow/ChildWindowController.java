package app.controller.childwindow;

import app.controller.mainwindow.MainWindowController;
import app.util.Util;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Scope("prototype")
public class ChildWindowController {
    private static final int WINDOW_WIDTH = 600;
    private static final int WINDOW_HEIGHT = 400;

    @Autowired
    private MainWindowController mainWindow;

    private Stage stage;

public ChildWindowController() {
    stage = new Stage();
    stage.centerOnScreen();
    Util.setStageIcon(stage, "Child Window : ");
}
    @PostConstruct
    public void init()  throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/childwindow/childWindowView.fxml"));
        loader.setController(this);
        Parent rootNode = loader.load();

        Scene scene = new Scene(rootNode, WINDOW_WIDTH, WINDOW_HEIGHT);
        stage.setScene(scene);
        stage.initOwner(mainWindow.getStage());
    }

    @FXML
    public void initialize() {
        System.out.println("! ChildWindowController.initialize()");
    }

    public void show() {
        stage.show();
    }
}

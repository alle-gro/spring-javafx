package app.service;

import app.domain.Contact;
import app.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContactServiceImpl implements ContactService {
    @Autowired
    ContactRepository repositoty;

    @Override
    public List<Contact> findAll() {
        return (List<Contact>) repositoty.findAll();
    }

    @Override
    public Optional<Contact> findById(Long id) {
        return repositoty.findById(id);
    }

    @Override
    public Contact save(Contact contact) {
        return repositoty.save(contact);
    }

    @Override
    public void delete(Contact contact) {
        repositoty.delete(contact);
    }
}

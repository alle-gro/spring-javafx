create sequence hibernate_sequence start 1 increment 1;

create table contact (
    id bigint auto_increment,
    name varchar(255),
    phone varchar(255),
    email varchar(255) not null,
    primary key (id)
);


